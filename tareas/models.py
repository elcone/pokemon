from math import trunc

from django.db import models
from django.contrib.auth.models import User


class ProyectoQueryset(models.query.QuerySet):
    def con_avance(self):
        return self.annotate(models.Avg('tareas__avance'))


class ProyectoManager(models.Manager):
    def get_queryset(self):
        return ProyectoQueryset(self.model, using=self._db)

    def con_avance(self):
        return self.get_queryset().con_avance()


class Proyecto(models.Model):
    propietario = models.ForeignKey(User, related_name='proyectos')
    nombre = models.CharField(max_length=100)

    objects = ProyectoManager()

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = 'proyectos'

    def avance(self):
        avance = self.tareas.aggregate(models.Avg('avance'))
        return int(avance['avance__avg'])

    def __str__(self):
        return self.nombre


class TareaQueryset(models.query.QuerySet):
    def terminadas(self):
        return self.filter(terminada=True)

    def pendientes(self):
        return self.filter(terminada=False)


class TareaManager(models.Manager):
    def get_queryset(self):
        return TareaQueryset(self.model, using=self._db)

    def terminadas(self):
        return self.get_queryset().terminadas()

    def pendientes(self):
        return self.get_queryset().pendientes()


class Tarea(models.Model):
    propietario = models.ForeignKey(User, related_name='tareas')
    proyecto = models.ForeignKey(Proyecto, related_name='tareas',
        null=True, blank=True)
    descripcion = models.CharField(max_length=100)
    detalles = models.TextField(null=True, blank=True)
    entrega = models.DateField(null=True, blank=True)
    terminada = models.BooleanField(default=False)
    avance = models.IntegerField(default=0)

    objects = TareaManager()

    class Meta:
        ordering = ['descripcion']
        verbose_name_plural = 'tareas'

    def save(self, *args, **kwargs):
        self.terminada = self.avance == 100
        super(Tarea, self).save(*args, **kwargs)

    def __str__(self):
        return self.descripcion
