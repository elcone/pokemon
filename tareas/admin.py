from django.contrib import admin
from .models import Proyecto, Tarea

admin.site.site_header = 'Proyectos y tareas'

@admin.register(Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'propietario']
    list_filter = ['propietario']
    search_fields = ['nombre']


@admin.register(Tarea)
class TareaAdmin(admin.ModelAdmin):
    list_display = ['descripcion', 'proyecto', 'propietario',
        'terminada', 'avance']
    list_filter = ['propietario', 'proyecto', 'terminada']
    search_fields = ['descripcion', 'detalles']
