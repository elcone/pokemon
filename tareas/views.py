from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy

from .models import Proyecto, Tarea

class ProyectoListView(ListView):
    model = Proyecto
    context_object_name = 'proyectos'


class ProyectoDetailView(DetailView):
    model = Proyecto
    context_object_name = 'proyecto'


class ProyectoCreateView(CreateView):
    model = Proyecto
    fields = ('propietario', 'nombre')
    success_url = reverse_lazy('proyectos')


class ProyectoUpdateView(UpdateView):
    model = Proyecto
    fields = ('nombre',)
    success_url = reverse_lazy('proyectos')
    context_object_name = 'proyecto'


class ProyectoDeleteView(DeleteView):
    model = Proyecto
    success_url = reverse_lazy('proyectos')
    context_object_name = 'proyecto'


class TareaListView(ListView):
    model = Tarea
    context_object_name = 'tareas'


class TareaCreateView(CreateView):
    model = Tarea
    fields = ('propietario', 'proyecto', 'descripcion', 'detalles',)
    success_url = reverse_lazy('tareas')


class TareaUpdateView(UpdateView):
    model = Tarea
    fields = ('descripcion', 'detalles', 'avance')
    success_url = reverse_lazy('tareas')
    context_object_name = 'tarea'


class TareaDeleteView(DeleteView):
    model = Tarea
    success_url = reverse_lazy('tareas')
    context_object_name = 'tarea'
