from django.conf.urls import url
from django.views.generic import TemplateView

from .views import (ProyectoListView, ProyectoDetailView, ProyectoCreateView,
    ProyectoUpdateView, ProyectoDeleteView, TareaListView, TareaCreateView,
    TareaUpdateView, TareaDeleteView)

urlpatterns = [
    url(r'proyectos/eliminar/(?P<pk>[0-9]+)/$', ProyectoDeleteView.as_view(),
        name='proyecto_eliminar'),
    url(r'proyectos/actualizar/(?P<pk>[0-9]+)/$', ProyectoUpdateView.as_view(),
        name='proyecto_actualizar'),
    url(r'proyectos/nuevo/$', ProyectoCreateView.as_view(), name='proyecto_nuevo'),
    url(r'proyectos/(?P<pk>[0-9]+)/$', ProyectoDetailView.as_view(), name='proyecto'),
    url(r'proyectos/$', ProyectoListView.as_view(), name='proyectos'),

    url(r'tareas/eliminar/(?P<pk>[0-9]+)/$', TareaDeleteView.as_view(),
        name='tarea_eliminar'),
    url(r'tareas/actualizar/(?P<pk>[0-9]+)/$', TareaUpdateView.as_view(),
        name='tarea_actualizar'),
    url(r'tareas/nueva/$', TareaCreateView.as_view(), name='tarea_nueva'),
    url(r'tareas/$', TareaListView.as_view(), name='tareas')
]
